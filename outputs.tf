output "public_ip" {
    description = "public ip"
  value = aws_instance.web.public_ip
}

#This output will get the public Ip from aws instance {web}